var mysql = require('mysql2');
var db = mysql.createConnection({
    host: process.env.MYSQL_HOST || "localhost",
    user: process.env.MYSQL_USER || "root",
    password: process.env.MYSQL_PASSWORD || "",
    database: process.env.MYSQL_DATABASE || "mekanikku"
});

db.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

module.exports = db.promise()