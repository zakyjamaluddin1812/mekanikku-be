const Sequelize = require('sequelize');


const host = process.env.MYSQL_HOST || "localhost"
const user = process.env.MYSQL_USER || "root"
const password = process.env.MYSQL_PASSWORD || ""
const database = process.env.MYSQL_DATABASE || "mekanikku"




const sequelize = new Sequelize(database, user, password, {
    host: host,
    dialect: "mysql"
  });

sequelize.authenticate().then(() => {
  console.log('Connection established successfully.');
}).catch(err => {
  console.error('Unable to connect to the database:', err);
})
let Users = sequelize.define('users', {
    noHp: Sequelize.STRING,
    password: Sequelize.STRING,
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    profile: Sequelize.STRING,
});

Users.sync().then(() => {
    console.log('New table created');
}).finally(() => {
    sequelize.close();
})

module.exports = sequelize