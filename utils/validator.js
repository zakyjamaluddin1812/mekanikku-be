const md5 = require('md5');

const Joi = require('joi');

const schema = Joi.object({
    username : Joi.string().min(9).max(14).required(),
    password : Joi.string().min(8).required()
})


const validasi = (user) => {
    try {
        return schema.validate(user);
    }
    catch (error) {
        throw error
    } 
}
module.exports = validasi