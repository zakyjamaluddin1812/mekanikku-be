var db = require("../utils/database");
var migration = require("../utils/migration");

const getUsers = async () => {
    const [users] = await db.query('SELECT * FROM users')
    return users
};


// Method global untuk kedua paramenter

const getUser = async (username, passwordHash) => {
    const [users] = await db.query(`SELECT * FROM users WHERE noHp = ? OR email = ?`, [username, username])
    if(users[0].password == passwordHash) {
        return users[0]
    } else {
        return null
    }
    
}



module.exports = {
    getUsers,
    getUser
}