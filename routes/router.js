const express = require('express')

const router = express.Router();
const index = require('../controllers/login')

// respond with "hello world" when a GET request is made to the homepage
router.post('/auth/v1/login', index)

module.exports = router