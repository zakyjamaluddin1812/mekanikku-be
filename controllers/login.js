
var db = require("../utils/database");
const userModel = require('../models/user')
const jwt = require('jsonwebtoken')
const md5 = require('md5')

const validator = require('../utils/validator');

const index = async (req, res) => {
    const username = req.body.username
    const password = req.body.password

    const validasi = validator(req.body)
    if(validasi.error) {
        res.status(401)
        res.json(
                    {
                     message: validasi.error.details[0].message,
                     code: 401
                    })
    } else {
        const passwordHash = md5(password)
        const user = await userModel.getUser(username, passwordHash)
        if (user == null) {
            res.status(401)
            res.json(
                        {
                        message: "User not found",
                        code: 401
                        })
        } else {
            const token = jwt.sign(
                {
                    username : username,
                    password : password
                },
                '12345',
                {
                    expiresIn : "2h"
                }
                )
                res.status(200)
                res.json(
                            {
                             data: token,
                             message: "Success",
                             code: 200
                            })
            
        }
    }
}
 module.exports = index